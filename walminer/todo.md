**DONE TEMP**
**INDO:**
 - 修改条件语句，设置模式可以增加tcid，在有主键的情况下可以只显示主键条件
 - 修改update语句，可以只显示更新的数据
 - 实现自apply解析(主要为故障转移后主备延迟情况提供解决方案)
   1.测试子事务失败的情况
   2.测试失败后，不commit会怎样
   3.解决本地测试self_apply时wal迭代生成问题--无需解决因为一遍扫描既定了结束位置
   4.wal日志自动添加问题
   5.测试子事务失败后，其父事务会不会abort
   6.没有主键的表需要增加仅删除一条记录的限制
 - DDL解析基础
   1.inplace update为支持
   2.系统表最终结果的开始lsn为第一个，匹配子DML语句的开始lsn

**ONDO：**
 - image读写机制设计(效率)->预读线程记录事务信息，
   解析进程直接插入数据或摒除不需要的记录，这样就不再需要事务信息落盘存储
 - 分析其他事务wal类型
 - 开发wal各类型长度统计
 - 改为unloged表,并添加多个事务分段输出结果
 

**TODO:**
 - toast数据存盘处理(目前只对普通元组有存盘处理，未写toast的存盘处理)
 - 简易解析DDL语句


**测试项：**

**限制项：**

1. 当前walminer无法处理数据字典不一致问题，walminer始终以给定的数据字典为准，
   对于无法处理的relfilenode，那么会丢弃这一条wal记录(会有一个notice在解析结果中没有体现)
2. complete属性只有在wallevel大于minimal时有效
3. xid解析模式不支持子事务
4. 同时只能有一个walminer解析进程，否则会出现解析混乱
5. 化身表不支持toast,化身表与原表结构不一致时会产生崩溃
6. DDL解析必须在精确解析模式中进行


**升级项**
